package com.example.atividade2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class MensagemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mensagem);
        TextView textViewMensagem = findViewById(R.id.textViewMensagem);
        textViewMensagem.setText(MainActivity.activity.getMensagem());
    }
}