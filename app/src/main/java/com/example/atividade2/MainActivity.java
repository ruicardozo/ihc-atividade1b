package com.example.atividade2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public static MainActivity activity;
    private EditText editTextMensagem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;

        editTextMensagem = findViewById(R.id.editTextTextMensagem);
        Button buttonEnviar = findViewById(R.id.buttonEnviar);

        buttonEnviar.setOnClickListener(view -> {
            startActivity(new Intent(this, MensagemActivity.class));
        });
    }

    public String getMensagem()
    {
        return editTextMensagem.getText().toString();
    }
}